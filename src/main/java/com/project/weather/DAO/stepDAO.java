package com.project.weather.DAO;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.project.weather.DTO.stepVo;

public interface stepDAO {
	public List<stepVo> selectByOne(@Param("one")String one) throws Exception;
	public List<stepVo> selectByOneTwo(@Param("one")String one, @Param("two")String two) throws Exception;
	public stepVo selectByOneTwoThree(@Param("one")String one, @Param("two")String two, @Param("three")String three) throws Exception;
	public void insert(@Param("stepVo") stepVo stepVo) throws Exception;
}
