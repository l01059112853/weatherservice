package com.project.weather.DAO;

import com.project.weather.DTO.latlngVo;

public interface latlngDAO {
	public Object[] selectByNo(int no) throws Exception;
	public void insert(latlngVo latlngVo) throws Exception;
}
