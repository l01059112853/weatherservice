package com.project.weather.DTO;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * HTTP 요청에 대한 정보를 가지고 있는 Vo
 * 
 * @author 이민혁
 *
 */
public class HttpVo {
	// remote --> Client
	private HttpServletRequest request;
	private String remoteAddr;
	private String remoteHost;
	private String remotePort;
	private String url;
	private String uri;
	private String port;
	private Map<String, Object> headers;
	private Map<String, Object> params;

	public HttpVo(HttpServletRequest request) {
		this.url = request.getRequestURL().toString();
		this.uri = request.getRequestURI().toString();
		this.port = Integer.toString(request.getServerPort());
		this.remoteAddr = request.getRemoteAddr().toString();
		this.remoteHost = request.getRemoteHost().toString();
		this.remotePort = Integer.toString(request.getRemotePort());
		headers = new HashMap<String, Object>();
		params = new HashMap<String, Object>();

		if (request != null) {
			Enumeration<String> enums = request.getHeaderNames();
			while (enums.hasMoreElements()) {
				String headerName = enums.nextElement();
				String value = request.getHeader(headerName);
				headers.put(headerName, value);
			}
		}
		
		if (params != null) {
			Enumeration<String> enums = request.getParameterNames();
			while(enums.hasMoreElements()) {
				String paramName = enums.nextElement();
				String value = request.getParameter(paramName);
				params.put(paramName, value);
			}
		}
		
		System.out.println("[ " + this.getClass().toString() + " ] headers --> " + headers.toString());
		System.out.println("[ " + this.getClass().toString() + " ] params --> " + params.toString());
	}
	
	public HttpVo(String url, String uri, String port, String remoteAddr, String remoteHost, String remotePort,
			Map<String, Object> headers, Map<String, Object> params) {
		this.url = url;
		this.uri = uri;
		this.port = port;
		this.remoteAddr = remoteAddr;
		this.remoteHost = remoteHost;
		this.remotePort = remotePort;
		this.headers = headers;
		this.params = params;
	}

	public String getRemoteAddr() {
		return remoteAddr;
	}

	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}

	public String getRemoteHost() {
		return remoteHost;
	}

	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}

	public String getRemotePort() {
		return remotePort;
	}

	public void setRemotePort(String remotePort) {
		this.remotePort = remotePort;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public Map<String, Object> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, Object> headers) {
		this.headers = headers;
	}
	
	public Map<String, Object> getParams(){
		return params;
	}
	
	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	@Override
	public String toString() {
		return "HttpVo [remoteAddr=" + remoteAddr + ", remoteHost=" + remoteHost + ", remotePort=" + remotePort
				+ ", url=" + url + ", uri=" + uri + ", port=" + port + "]\n" + headers.toString() + "\n" + params.toString();
	}
}
