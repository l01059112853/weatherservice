package com.project.weather.DTO;

public class latlngVo {
	int no;
	String X;
	String Y;
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getX() {
		return X;
	}
	public void setX(String x) {
		X = x;
	}
	public String getY() {
		return Y;
	}
	public void setY(String y) {
		Y = y;
	}
	
	@Override
	public String toString() {
		return "latlngVo [no=" + no + ", X=" + X + ", Y=" + Y + "]";
	}
}
