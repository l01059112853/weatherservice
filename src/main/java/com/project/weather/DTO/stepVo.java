package com.project.weather.DTO;

public class stepVo {
	int no;
	String one;
	String two;
	String three;
	latlngVo latlngVo;
	
	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getOne() {
		return one;
	}

	public void setOne(String one) {
		this.one = one;
	}

	public String getTwo() {
		return two;
	}

	public void setTwo(String two) {
		this.two = two;
	}

	public String getThree() {
		return three;
	}

	public void setThree(String three) {
		this.three = three;
	}

	public latlngVo getLatlngVo() {
		return latlngVo;
	}

	public void setLatlngVo(latlngVo latlngVo) {
		this.latlngVo = latlngVo;
	}

	@Override
	public String toString() {
		
		if(this.latlngVo != null)
			return "stepVo [no=" + no + ", one=" + one + ", two=" + two + ", three=" + three + "]\nlatlngVo [X=" + latlngVo.X + ", Y=" + latlngVo.Y + "]";
		else
			return "stepVo [no=" + no + ", one=" + one + ", two=" + two + ", three=" + three + "]";
	}
}
