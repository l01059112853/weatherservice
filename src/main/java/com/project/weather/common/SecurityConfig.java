package com.project.weather.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private static final Logger logger = LoggerFactory.getLogger(SecurityConfig.class);
	
	
	/**
	 * URL에 적절한 HTTP 혹은 HTTPS를 매칭하기 위한 Configure
	 * 
	 * <pre>
	 * <b>http.requiresChannel()</b>
	 * .requiresSecure() : HTTPS 접근
	 * .requiresInsecure() : HTTP 접근
	 * </pre>
	 * */
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//http.requiresChannel().antMatchers("/*").requiresSecure();
		http.portMapper().http(8080).mapsTo(8443);
		http.csrf().disable();
		
		/*
		 * http.authorizeRequests().antMatchers("/*").permitAll();
		 * --> https 요청만 허용 
		 */
	}
}
