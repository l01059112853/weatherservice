package com.project.weather.service;

import com.project.weather.DTO.latlngVo;

public interface latlngService {
	public Object[] selectByNo(int no) throws Exception;
	public void insert(latlngVo latlngVo) throws Exception;
}
