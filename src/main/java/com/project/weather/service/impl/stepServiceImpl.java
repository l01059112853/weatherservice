package com.project.weather.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.weather.DAO.stepDAO;
import com.project.weather.DTO.stepVo;
import com.project.weather.service.stepService;

@Service
public class stepServiceImpl implements stepService {
	
	@Autowired
	stepDAO stepDAO;

	@Override
	public List<stepVo> selectByOne(String one) throws Exception {
		return stepDAO.selectByOne(one);
	}

	@Override
	public List<stepVo> selectByOneTwo(String one, String two) throws Exception {
		return stepDAO.selectByOneTwo(one, two);
	}

	@Override
	public stepVo selectByOneTwoThree(String one, String two, String three) throws Exception {
		return stepDAO.selectByOneTwoThree(one, two, three);
	}

	@Override
	public void insert(stepVo stepVo) throws Exception {
		stepDAO.insert(stepVo);
	}

}
