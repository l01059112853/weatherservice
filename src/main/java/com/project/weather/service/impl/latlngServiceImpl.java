package com.project.weather.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.weather.DAO.latlngDAO;
import com.project.weather.DTO.latlngVo;
import com.project.weather.service.latlngService;

@Service
public class latlngServiceImpl implements latlngService {
	
	@Autowired
	latlngDAO latlngDAO;

	@Override
	public Object[] selectByNo(int no) throws Exception {
		return latlngDAO.selectByNo(no);
	}

	@Override
	public void insert(latlngVo latlngVo) throws Exception {
		latlngDAO.insert(latlngVo);
	}

}
