package com.project.weather.service;

import java.util.List;

import com.project.weather.DTO.stepVo;

public interface stepService {
	public List<stepVo> selectByOne(String one) throws Exception;
	public List<stepVo> selectByOneTwo(String one, String two) throws Exception;
	public stepVo selectByOneTwoThree(String one, String two, String three) throws Exception;
	public void insert(stepVo stepVo) throws Exception;
}
