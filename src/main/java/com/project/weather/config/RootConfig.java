package com.project.weather.config;

import javax.validation.Validator;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@ComponentScan(basePackages= { "com.project.weather"} )
@MapperScan(basePackages= {"com.project.weather.DAO"})

@EnableWebMvc
@EnableScheduling
@EnableTransactionManagement
public class RootConfig {
	
	@Bean
	public DataSource dataSource() {
		DataSource ds = new DataSource();
		
		// log4jdbc를 통해 SQL쿼리문이 Console에 나타내도록 하기 위한 설정
		ds.setDriverClassName("net.sf.log4jdbc.sql.jdbcapi.DriverSpy");
		ds.setUrl("jdbc:log4jdbc:mariadb://13.125.129.138:3306/weather?serverTimezone=Asia/Seoul&allowMultiQueries=true");
		ds.setUsername("minh317");
		ds.setPassword("1234");
		ds.setInitialSize(2);
		ds.setMaxActive(10);
		ds.setMaxIdle(10);
		
		// ValidationQuery
		// 일정시간(디폴트 8hours)마다 DB에 요청이 없으면 커넥션을 자동으로 끊어버리게 된다
		// 이를 예방하기 위해 특정 시간별로 요청을 줌으로써 커넥션이 끊기지 않도록 만든다.

		ds.setTestWhileIdle(true);
		ds.setValidationQuery("select 1");
		ds.setMinEvictableIdleTimeMillis(60000*3);
		ds.setTimeBetweenEvictionRunsMillis(60*1000);
		ds.setValidationQueryTimeout(2880);
		
		return ds;
	}
	
	@Bean
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
		sqlSessionFactory.setDataSource(dataSource());
		
		return (SqlSessionFactory)sqlSessionFactory.getObject();
	}
	
	@Bean
	public Validator localValidatorFactoroyBean() {
		return new LocalValidatorFactoryBean();
	}
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
