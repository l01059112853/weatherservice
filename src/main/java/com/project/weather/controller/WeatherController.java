package com.project.weather.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.project.weather.DAO.stepDAO;
import com.project.weather.DTO.HttpVo;
import com.project.weather.DTO.stepVo;

import io.jsonwebtoken.ClaimJwtException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.PrematureJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 지역 별 위도, 경도를 구해오는 API
 * 
 * @author 이민혁
 * 
 *         <pre>
 * <b>Header</b>
 * CONTENT-TYPE: application/JSON (default) required=true
 * AUTHORIZATION: BAERER AAA.BBB.CCC required=false
 * 
 * <b>Params</b>
 * one(시/도) required=false
 * two(구/군) required=false
 * three(읍/면/동) required=false
 *         
 * <b>* AUTHORIZATION 혹은 one 둘 중 하나는 반드시 기입이 되어야 함.</b>
 *         </pre>
 */
@RestController
@Component
@PropertySource({"classpath:properties/${dev.position}/application.properties"})
public class WeatherController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	stepDAO stepDAO;
	
	@Value("${jwt.secret}")
	private String secretKey;
	
	@ApiOperation(value = "JWT 생성 API")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "Header", name="CONTENT-TYPE", value="application/JSON", required=true, defaultValue="application/JSON"),
		@ApiImplicitParam(paramType = "Header", name="EXPIRATION", value="expiration", required=true),
		@ApiImplicitParam(paramType = "query", value = "시/도", name = "one", required = true),
		@ApiImplicitParam(paramType = "query", value = "구/군", name = "two", required = false),
		@ApiImplicitParam(paramType = "query", value = "읍/면/동", name = "three", required = false) })		
	@RequestMapping(value = "/jwt", method = RequestMethod.GET)
	public @ResponseBody Object jwt(HttpServletRequest request) throws Exception {
		
		logger.debug("[ " + this.getClass().toString() + " ] Jwt API Request...");
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, Object>> rtnList = new ArrayList<>(); // 반환할 List
		
		HttpVo httpVo = (HttpVo)request.getAttribute("httpVo");
		
		if(httpVo == null) {
			map.put("Error", "Could not Create Token.");
			map.put("Cause", "HTTP request is invalid.");
			return map;
		}
		
		int expired = Integer.parseInt((String)httpVo.getHeaders().get("expiration")) * 1000;
		if(expired <= 0) {
			map.put("Error", "Could not Create Token.");
			map.put("Cause", "Expiration is invalid.");
			return map;
		}
		
		String one = (String)httpVo.getParams().get("one");
		String two = (String)httpVo.getParams().get("two");
		String three = (String)httpVo.getParams().get("three");
		
		if(one == null) {
			map.put("Error", "Could not Create Token.");
			map.put("Cause", "Parameter is invalid.");
			return map;
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		Date curDate = new Date(System.currentTimeMillis());
		Date expDate = new Date(System.currentTimeMillis() + (expired)); // 30minutes
		
		String jwt = Jwts.builder()
				.setHeaderParam("typ", "JWT")
				.setExpiration(expDate)
				.claim("one", one)
				.claim("two", two)
				.claim("three", three)
				.signWith(SignatureAlgorithm.HS256, secretKey.getBytes("UTF-8")).compact();
		
		map.put("Request Time", sdf.format(curDate));
		map.put("Expired Time", sdf.format(expDate));
		map.put("jwt", jwt);
		
		rtnList.add(map);
		return rtnList;
	}

	@ApiOperation(value = "지역 별 위도 및 경도 구하기 API")
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "Header", name = "CONTENT-TYPE", value = "application/JSON", required = true, defaultValue = "application/JSON"),
			@ApiImplicitParam(paramType = "Header", name = "AUTHORIZATION", value = "JWT", required = false),
			@ApiImplicitParam(paramType = "query", value = "시/도", name = "one", required = false),
			@ApiImplicitParam(paramType = "query", value = "구/군", name = "two", required = false),
			@ApiImplicitParam(paramType = "query", value = "읍/면/동", name = "three", required = false) })
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public @ResponseBody Object home(HttpServletRequest request) throws Exception {

		String one = "", two = "", three = "";
		Map<String, Object> map = new HashMap<String, Object>();

		final String secretKey = (String) request.getAttribute("secretKey");
		final boolean required = (boolean) request.getAttribute("required_jwtParse");

		logger.debug("[ " + this.getClass().toString() + " ] secretKey --> " + secretKey);

		if (required == false) {
			one = request.getParameter("one");
			two = request.getParameter("two");
			three = request.getParameter("three");
		} else {
			try {
				final String jwt = (String) request.getAttribute("jwt");
				Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey.getBytes("UTF-8")).parseClaimsJws(jwt);

				logger.debug("[ " + this.getClass().toString() + " ] jws --> " + jwt);
				logger.debug("[ " + this.getClass().toString() + " ] claims --> " + claims.getBody().toString());

				one = (String) claims.getBody().get("one");
				two = (String) claims.getBody().get("two");
				three = (String) claims.getBody().get("three");
			} catch (MalformedJwtException | ExpiredJwtException | PrematureJwtException | SignatureException
					| UnsupportedJwtException e) {
				logger.debug(e.getMessage());

				map.put("Error", "Could not Search Data.");
				map.put("Cause", e.getMessage());
				return map;
			}
		}

		if (one == null || "".equals(one)) {
			map.put("Error", "Could not Search Data.");
			map.put("Cause", "Not Exists Parameter.");
			return map;
		}

		List<Map<String, Object>> rtnList = new ArrayList<>(); // 반환할 List
		List<stepVo> list = null; // 조회한 데이터를 담을 List

		// one(시/도)만 입력한 경우
		if (two == null && three == null) {
			list = stepDAO.selectByOne(one);

			if (list.size() <= 0) {
				map.put("Error", "Could not Search Data.");
				map.put("Cause", "There is no data that matchs the criteria.");
				return map;
			}

			for (stepVo vo : list) {
				Map<String, Object> inMap = new HashMap<String, Object>();

				inMap.put("ONE", vo.getOne());
				if (!(vo.getTwo().equals("")) && !(vo.getTwo() == null))
					inMap.put("TWO", vo.getTwo());

				if (!(vo.getThree().equals("")) && !(vo.getThree() == null))
					inMap.put("THREE", vo.getThree());

				inMap.put("X", vo.getLatlngVo().getX());
				inMap.put("Y", vo.getLatlngVo().getY());

				rtnList.add(inMap);
			}

			return rtnList;
		} else if (two != null && three == null) {
			list = stepDAO.selectByOneTwo(one, two);

			if (list.size() <= 0) {
				map.put("Error", "Could not Search Data.");
				map.put("Cause", "There is no data that matchs the criteria.");
				return map;
			}

			for (stepVo vo : list) {
				Map<String, Object> inMap = new HashMap<String, Object>();

				inMap.put("ONE", vo.getOne());
				if (!(vo.getTwo().equals("")) && !(vo.getTwo() == null))
					inMap.put("TWO", vo.getTwo());

				if (!(vo.getThree().equals("")) && !(vo.getThree() == null))
					inMap.put("THREE", vo.getThree());

				inMap.put("X", vo.getLatlngVo().getX());
				inMap.put("Y", vo.getLatlngVo().getY());

				rtnList.add(inMap);
			}

			return rtnList;
		} else if (two != null && three != null) {
			stepVo stepVo = stepDAO.selectByOneTwoThree(one, two, three);

			if (stepVo == null) {
				map.put("Error", "Could not Search Data.");
				map.put("Cause", "There is no data that matchs the criteria.");
				return map;
			}

			map.put("ONE", stepVo.getOne());
			map.put("TWO", stepVo.getTwo());
			map.put("THREE", stepVo.getThree());
			map.put("X", stepVo.getLatlngVo().getX());
			map.put("Y", stepVo.getLatlngVo().getY());
			rtnList.add(map);

			return rtnList;
		} else {
			map.put("Error", "Could not Search Data.");
			map.put("Cause", "Not Exists Parameter.");
			return map;
		}
	}

}
