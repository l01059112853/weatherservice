package com.project.weather.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.project.weather.DTO.HttpVo;

public class WeatherInterceptor extends HandlerInterceptorAdapter {

	private final String secretKey;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public WeatherInterceptor(String secretKey) {
		this.secretKey = secretKey;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		logger.debug("[ " + this.getClass().toString() + " ] Request URL --> " + request.getRequestURL());

		HttpVo httpVo = getHttpVo(request);

		if (httpVo == null) {
			logger.debug("httpVo is null");
			return false;
		} else {
			logger.debug("[ " + this.getClass().toString() + " ] secretKey --> " + secretKey);

			String authorization = (String)httpVo.getHeaders().get("authorization");

			if (authorization == null || authorization.equals("")) {
				
				 /*
				 * Create Jwt
				 * String jwt = Jwts.builder().setHeaderParam("typ", "JWT") .setExpiration(new
				 * Date(System.currentTimeMillis() + (1000 * 3600))) // expireTime --> 1 hour
				 * .claim("one", one).claim("two", two).claim("three", three)
				 * .signWith(SignatureAlgorithm.HS256, secretKey.getBytes("UTF-8")).compact();
				 */
				
				request.setAttribute("required_jwtParse", false);
			} else {
				request.setAttribute("jwt", authorization);
				request.setAttribute("required_jwtParse", true);
			}

			request.setAttribute("httpVo", httpVo);
			request.setAttribute("secretKey", secretKey);
			return super.preHandle(request, response, handler);
		}
	}

	/**
	 * @param request
	 * @return return ReqVo with HTTP Reuqest.
	 */
	public HttpVo getHttpVo(HttpServletRequest request) {
		if (request != null)
			return new HttpVo(request);
		else
			return null;
	}

}
