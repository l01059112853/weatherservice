package com.project.weather.util;

import java.io.FileReader;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.project.weather.DTO.latlngVo;
import com.project.weather.DTO.stepVo;


public class jsonParse {
	
	public static void main(String[] args) {
		
		int sucCnt = 0;
		int nullCnt = 0;
		
		JSONParser parser = new JSONParser();
		stepVo stepVo = new stepVo();
		latlngVo latlngVo = new latlngVo();
		
		try {
			JSONArray jsonArray = (JSONArray) parser.parse(new FileReader("C:\\Spring\\gitlab\\project\\WeatherService\\src\\main\\webapp\\resources\\json\\latlng.json"));
			
			for(int i = 0; i < jsonArray.size(); i++) {
				JSONObject object = (JSONObject)jsonArray.get(i);
				
				String one = object.get("1단계").toString();
				String two = object.get("2단계").toString();
				String three = object.get("3단계").toString();
				String X = object.get("격자 X").toString();
				String Y = object.get("격자 Y").toString();
				
				if(one.equals("") || X.equals("") || Y.equals("")) {
					nullCnt++;
					System.out.println("** No Data **");
					continue;
				}
				
				stepVo.setOne(object.get("1단계").toString());
				latlngVo.setX(X); latlngVo.setY(Y);
				
				if(two.equals("")) {
					stepVo.setTwo("");
				}
				else {
					stepVo.setTwo(two);
				}
				
				if(three.equals("")) {
					stepVo.setThree("");
				}
				else {
					stepVo.setThree(three);
				}
				
				sucCnt++;
				stepVo.setLatlngVo(latlngVo);
				
				System.out.println(stepVo.toString());
			}
			
			System.out.println("** success : " + sucCnt);
			System.out.println("** fail : " + nullCnt);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
