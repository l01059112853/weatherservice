package com.project.weather.Test;

import java.io.FileReader;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.project.weather.DAO.stepDAO;
import com.project.weather.DTO.latlngVo;
import com.project.weather.DTO.stepVo;
import com.project.weather.config.RootConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { RootConfig.class })
public class DbConnectTest {

	public static final Logger logger = LoggerFactory.getLogger(DbConnectTest.class);

	@Autowired
	stepDAO stepDAO;

	@Test
	public void test() throws Exception {

		/*
		 * int sucCnt = 0; int nullCnt = 0;
		 * 
		 * JSONParser parser = new JSONParser(); stepVo stepVo = new stepVo(); latlngVo
		 * latlngVo = new latlngVo();
		 * 
		 * JSONArray jsonArray = (JSONArray) parser.parse(new FileReader(
		 * "C:\\Spring\\gitlab\\project\\WeatherService\\src\\main\\webapp\\resources\\json\\latlng.json"
		 * )); for(int i = 0; i < jsonArray.size(); i++) { JSONObject object =
		 * (JSONObject)jsonArray.get(i);
		 * 
		 * String one = object.get("1단계").toString(); String two =
		 * object.get("2단계").toString(); String three = object.get("3단계").toString();
		 * String X = object.get("격자 X").toString(); String Y =
		 * object.get("격자 Y").toString();
		 * 
		 * if(one.equals("") || X.equals("") || Y.equals("")) { nullCnt++;
		 * System.out.println("** No Data **"); continue; }
		 * 
		 * stepVo.setOne(object.get("1단계").toString()); latlngVo.setX(X);
		 * latlngVo.setY(Y);
		 * 
		 * if(two.equals("")) { stepVo.setTwo(""); } else { stepVo.setTwo(two); }
		 * 
		 * if(three.equals("")) { stepVo.setThree(""); } else { stepVo.setThree(three);
		 * }
		 * 
		 * sucCnt++; stepVo.setLatlngVo(latlngVo);
		 * 
		 * stepDAO.insert(stepVo); }
		 * 
		 * System.out.println("** success : " + sucCnt); System.out.println("** fail : "
		 * + nullCnt);
		 */

		
		 List<stepVo> lists = stepDAO.selectByOne("서울특별시");
		 for(stepVo vo : lists) {
			 System.out.println(vo.toString());
		 }
	}
}
